# Create a new Seafile share link using command line

usage: seaf-new-link path

## Dependencies

seaf-new-link depends on:
 
 * a working seaf-cli instance
 * python-seafile-webapi <https://framagit.org/flobiree/python-seafile-webapi>
